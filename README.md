# ADAN

**ADAN (Agent Disambiguation Author Name)** is a project that uses a multi-agent approach to disambiguate authors' names in digital bibliographic repositories.




1.0 - This version has been completely remodeled in Java with new features:

Parallel version of the SCI-synergy project available at: https://gitlab.com/InfoKnow/SocialNetwork/aureliocosta-sci-synergy

- New graph views
- Disambiguation of authors' names 

1.1 - DesambAgent - ADAN(Agent Disambiguation Author Name) Genesis

Name disambiguation module using a multi-agent approach with JADE. Made as a final project of the discipline "Multiagent Systems" offered by CIC / University of Brasília.

- It has the same objective as DesambModule (1.0), but uses a multi-agent strategy.
- New types of comparison and similarity of strings (Dice, Cosine) have been added

To run, compile and run "RunAgents.java" and configure your Neo4j database entries in the "AgenteMestre.java" file.

The documentation with the architectures is available in the folder "agent_arquitectures".

1.2 - ADAN -  Distributed version (Skiped to 1.3);

**1.3 - ADAN** - This multi-agent version can be used on distributed machines and has an optimized '[Teams and Workers](https://developers.google.com/optimization/assignment/assignment_teams)' model for deliberation of executing agents.

- To run this version you will have to unzip the file and create a Maven project with them.

- You must have a Neo4j database installed to run ADAN.
    
- This version allows for a distributed execution on two machines allowing faster execution and the creation of more agents for disambiguation.

- The main class for execution is RunADAN.java which is present in the package "adanRun".
    
- To run, compile and run "RunADAN.java" and configure your Neo4j database entries in the config file.
    
- There is a configuration file (adan_config.config) inside the "config" package. For execution, carefully read the instructions in the configuration file.

- To implement the ADAN, information from five Brazilian universities (UnB, UFMG, USP, UFRN and UFAM) is used. 
    
- At the end of the execution, information on authors and co-authorships will be made available in the Neo4j database. 

The ADAN v.13 architecture is shown in the following image:

![alt text1][logo]

[logo]: multi-agent_diagram.png "Title Text"

